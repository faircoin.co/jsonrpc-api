# Developer report

Here are some commands and infos of a real server preparation to gitlab-runner usage.

## 2021 Mar 28
## Running electrum-faircoin server on AIC (okamain with LXC=syncthing)
### using gitlab-runner with help from Sebastian

+ start from https://git.fairkom.net
+ fork https://git.fairkom.net/faircoin.co/electrumfairchainsx ( click fork button on top right and select the target of the fork )

+ new forked repo https://git.fairkom.net/example/electrumfairchainsx

+ edit https://git.fairkom.net/example/electrumfairchainsx/-/blob/master/.gitlab-ci.yml and change the gitlab-runner tags to yours ( for example your git group name, your server name, your domain, ... )

+ goto https://git.fairkom.net/example/electrumfairchainsx/-/settings/ci_cd
+ find the registration token in Settings -> CI/CD -> Runner ( section )
+ this token is needed when you register your gitlab-runner on your server
~~~
sudo gitlab-runner register
~~~
+ URL: enter only https://git.fairkom.net
+ tags: example.com
+ description: example.com runner
+ executor: shell

+ a gitlab-runner user will be created after registration

+ if you have registered it successfully then you need to add your gitlab-runner to docker group
~~~
sudo usermod -a -G docker gitlab-runner
~~~

~~~
### some helpful commands if you want list/remove runner ####
sudo gitlab-runner list
sudo gitlab-runner unregister runnersID

### alternativeley you can edit the /etc/gitlab-runner/config.toml
sudo nano /etc/gitlab-runner/config.toml
sudo gitlab-runner restart
~~~

+ then remove the clear section in /home/gitlab-runner/.bash_logout
~~~
sudo nano /home/gitlab-runner/.bash_logout
~~~

+ then set a password to get access to gitlab-runner later if you need to debug something
~~~
sudo passwd gitlab-runner
~~~

+ to execute pipelines you must go to CI/CD not Settings -> CI/CD
+ go in git -> CI/CD -> pipelines -> Run Pipeline ->
~~~
Variable => CMD = install
~~~

+ go to Settings -> CI/CD -> Variables -> create Type(File) SSL_CONF -> put a copy of the ./env_file/SSL_CONF and edit it


+ you can run it manually by ssh access log in as gitlab-runner
~~~
. setenv
deploy/install
~~~

+ at the beginning the faircoind must build up the blockchain database. Every new block must be checked if its valid. This requires a lot of #CPU power but it will be temporarily.

+ wait ~3h until the blockchain is up-to-date then your server should be available in electrumfair

+ If it is not available then check your DNS and firewall settings

~~~
### DNS check -> both commands should result the same ip
curl ifconfig.me
dig electrum.example.com
~~~

~~~
### Firewall check -> test it inside and outside of your server
telnet electrum.example.com 51812

### if it is accessible then it should result the line
Escape character is '^]'
~~~
