# jsonrpc-api

faircoin jsonrpc for php developers


## Deployment

### Requirements

+ vps server ( to prepare vps server you can find some helpful commands here https://git.fairkom.net/faircoin.co/admin/-/blob/master/SERVERSETUP.md )
+ docker installed ( https://docs.docker.com/engine/install/ )
+ docker-compose installed ( https://docs.docker.com/compose/install/ )
+ gitlab-runner installed ( optional for gitlab deployment ) ( see vps server setup )

## Usage

### by Gitlab-Runner

1. Fork this project to your group/repositories where you have set up a gitlab-runner
1. change the gitlab-runner tags in .gitlab-ci.yml
1. add env files ( select Type = file ! ) to override defaults ./env_file , go to Gitlab **Settings** -> **CI/CD** -> **Variables**

You can read a full developer report of server preparation with gitlab-runner usage > [DEV_REPORT.md](DEV_REPORT.md)

~~~
#### env files ( examples see env_file/... / select Type = file !) ######################
FAIRCHAINS_CONF                 # [optional] fairchains.conf / faircoin.conf
SSL_CONF                        # [mandatory] configuration to create ssl cert
FAIRCHAINS_ELECTRUMX_JSON       # [optional] electrumx serve network configuration
FAIRCHAINS_JSON                 # [optional] blockchain parameters
~~~
1. run command, go to Gitlab **CI/CD** -> **Pipeline** and **Run Pipeline**
Enter variable name **CMD**
~~~
build        # build container ( changes of Dockerfile )
start        # start container ( changes of scripts )
new_ssl      # create new ssl file for electrumx server
stop         # stop container
install      # runs build, start and new_ssl stages
uninstall    # remove container without to delete the data
remove       # remove all data
~~~

Wallet commands:
~~~
create wallet     # create new electrumfair wallet
restore wallet    # restore electrumfair wallet from seed or key
load wallet       # load wallet ( make it hot )
close wallet      # close wallet ( make it cold )
walletinfo        # get electrumfair wallet info
~~~

#### CI/CD Settings

Go Gitlab **Settings** -> **CI/CD** -> **Variables**

~~~
#### FairCoin.Co group variables ######################
LH_PORT_jsonrpc          # php -S 0.0.0.0:${LH_PORT}
~~~

Pipeline variables ( required to run wallet commands ):
~~~
CMD         = create wallet
WALLET      = <new wallet name>              # [optional] specify the wallets name
PASSPHRASE  = <new wallet passphrase>        # passphrase make your seed more secure
PASSWORD    = <new wallet password>          # password encrypts all wallets data on your drive
~~~

~~~
CMD         = restore wallet
SEED        = <your seed or key>             # seed, masterpubkey, masterprivkey, pubkey, privkey
WALLET      = <new wallet name>              # [optional] specify the wallets name
PASSPHRASE  = <new wallet passphrase>        # passphrase make your seed more secure
PASSWORD    = <new wallet password>          # password encrypts all wallets data on your drive
~~~

~~~
CMD         = load wallet
WALLET      = <wallet name>                  # [optional] specify the wallets name
PASSWORD    = <wallet password>              # password encrypts all wallets data on your drive
~~~

~~~
CMD         = close wallet
WALLET      = <wallet name>                  # [optional] specify the wallets name
PASSWORD    = <wallet password>              # password encrypts all wallets data on your drive
~~~


### by SSH access <small>( manually without gitlab-runner )</small>

1. install docker and docker-compose ( https://docs.docker.com/compose/install/ )
1. clone this project
1. change configuration in ./env_file
1. Initialize env vars
~~~
chmod +x setenv
. setenv
~~~

1. run command from project root folder
~~~
deploy/build
deploy/start
deploy/new_ssl
deploy/stop
deploy/install
deploy/uninstall
deploy/remove
~~~

#### Troubleshooting / console commands

some commands can be helpful getting shell access to container or get some status info
~~~
console/electrumx                   # get access to container of electrumfairchainsx server ( same as electrumfairchainsx )
console/electrumfairchainsx         # get access to container of electrumfairchainsx server
console/faircoin                    # get access to container of faircoind
console/php-server                  # get access to container of php-server

console/ps                          # list containers status info
console/status                      # list all ports / processes and faircoind info

console/create wallet               # create new electrumfair wallet
console/restore wallet              # restore electrumfair wallet from seed or key
console/load wallet                 # load wallet ( make it hot )
console/close wallet                # close wallet ( make it cold )
console/walletinfo                  # get electrumfair wallet info
~~~


## Nginx server setup

follow the instructions on https://git.fairkom.net/faircoin.co/admin/-/blob/master/SERVERSETUP.md#configure-server-host
