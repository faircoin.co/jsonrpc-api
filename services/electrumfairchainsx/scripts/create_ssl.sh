#!/bin/bash

#### create key and cert
openssl genrsa -out ~/.fairchains/electrumx.key 2048
openssl req -new -key ~/.fairchains/electrumx.key -out ~/.fairchains/electrumx.csr -config ~/scripts/ssl.conf

#### verify cert
openssl req -noout -text -in ~/.fairchains/electrumx.csr

#### sign cert
openssl x509 -req -days 1825 -in ~/.fairchains/electrumx.csr -signkey ~/.fairchains/electrumx.key -out ~/.fairchains/electrumx.crt
