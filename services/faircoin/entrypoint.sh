#!/usr/bin/env bash

# stop faircoind before container stops
stop_faircoind() {
  ./faircoin-cli stop > ./shutdown.log
  ./electrumfair/run_electrumfair daemon stop
}
trap 'stop_faircoind' SIGINT
trap 'stop_faircoind' SIGKILL
trap 'stop_faircoind' SIGTERM

if [ "${AUTOSTART}" == "1" ]
then
  ./faircoind -disablewallet -daemon
  if [ ! -d ./electrumfair/packages ]; then ./electrumfair/contrib/make_packages; fi
  rm ./electrumfair/electrumfair/servers.json
  cp ./servers.json ./electrumfair/electrumfair/
  ./electrumfair/run_electrumfair daemon start
fi

while true; do :; done
