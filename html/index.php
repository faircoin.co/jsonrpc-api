<html>
<head>
  <style>
    body {
      font-family: sans-serif;
      padding-bottom: 50px;
    }
    h4 {
      margin-top:5px;
      margin-bottom: 0px;
      color: grey;
      cursor: pointer;
    }
    h5 {
      margin-top:25px;
      margin-bottom: 0px;
      color: midnightblue;
    }
    h4 b {
      color: black;
    }
    .hidden {
      display: none;
    }
    textarea {
      width: 100%;
      padding: 10px;
      margin-top: 10px;
      margin-bottom: 20px;
    }
  </style>
  <script src="assets/js/jquery.min.js"></script>
</head>
<body>
  <h1>jsonrpc-php</h1>
  <p>Connect to faircoin blockchain via PHP by jsonrpc api. You can find all instructions and sources in <a href="https://git.fairkom.net/faircoin.co/jsonrpc-api" target="sources">git.fairkom.net</a></p>
<?php

include_once('functions/faircoin.php');

//$address='fJEtS3HDHxqX8Kpa2FChzGypPJYLBTRStq';
//$pubkey='04efbdb57592d1d4384a42a16dda525bc217ef7ce5e471ddf750e508096b5e9f5d99a3aa13a461ef4319417239dceae094351006a99134a14d43b32737f10fe405';
//$signature='HEbYHmm+QlB0aYlA/KK/2/lFqw1gM2yTzomUPjmLBFpwC7NM2iBk3R3aE3q+GaPtUUUPXprPPbETeOfThD+OI7c=';

$F = new Faircoin();

$txid='09268782af2883a829d99d4addd3a33142be0663199ebea2292ca791607c0bcc';
$message='Hello';

$address=$F->electrumfair->listaddresses()[0];
$pubkey=$F->electrumfair->getpubkeys($address)[0];
$signature=$F->electrumfair->signmessage($address,$message);


?><hr><h4>Example data</h4><?
?><textarea><?
echo json_encode(Array('address' => $address, 'pubkey' => $pubkey, 'message' => $message, 'encrypted' => $encrypted, 'signature' => $signature, 'txid' => $txid) );
?></textarea><?

?><hr><h2>System</h2><?
?><h4 onclick="$(this).next().toggleClass('hidden')">Faircoin->electrumfair-><b>version()</b></h4><?
?><textarea class="hidden"><?=json_encode($F->electrumfair->version())?></textarea><?

?><hr><h2>Message</h2><?
?><h4 onclick="$(this).next().toggleClass('hidden')">Faircoin->electrumfair-><b>signmessage($address,$message[,$password])</b></h4><?
?><textarea class="hidden"><?=json_encode($F->electrumfair->signmessage($address,$message))?></textarea><?

?><h4 onclick="$(this).next().toggleClass('hidden')">Faircoin->electrumfair-><b>verifymessage($address,$message,$signature)</b></h4><?
?><textarea class="hidden"><?=json_encode($F->electrumfair->verifymessage($address,$message,$signature))?></textarea><?

?><h4 onclick="$(this).next().toggleClass('hidden')">Faircoin->faircoind-><b>verifymessage($address,$message,$signature)</b></h4><?
?><textarea class="hidden"><?=json_encode($F->faircoind->verifymessage($address,$message,$signature))?></textarea><?

?><h4 onclick="$(this).next().toggleClass('hidden')">Faircoin->faircoind-><b>validatepubkey($pubkey)</b></h4><?
?><textarea class="hidden"><?=json_encode($F->faircoind->validatepubkey($pubkey))?></textarea><?



?><hr><h2>Address</h2><?
?><h4 onclick="$(this).next().toggleClass('hidden')">Faircoin->electrumfair-><b>getaddressbalance($address)</b></h4><?
?><textarea class="hidden"><?=json_encode($F->electrumfair->getaddressbalance($address))?></textarea><?

?><h4 onclick="$(this).next().toggleClass('hidden')">Faircoin->electrumfair-><b>getaddresshistory($address)</b></h4><?
?><textarea class="hidden"><?=json_encode($F->electrumfair->getaddresshistory($address))?></textarea><?

?><h4 onclick="$(this).next().toggleClass('hidden')">Faircoin->electrumfair-><b>getaddressunspent($address)</b></h4><?
?><textarea class="hidden"><?=json_encode($F->electrumfair->getaddressunspent($address))?></textarea><?

?><h4 onclick="$(this).next().toggleClass('hidden')">Faircoin->electrumfair-><b>validateaddress($address)</b></h4><?
?><textarea class="hidden"><?=json_encode($F->electrumfair->validateaddress($address))?></textarea><?

?><h4 onclick="$(this).next().toggleClass('hidden')">Faircoin->faircoind-><b>validateaddress($address)</b></h4><?
?><textarea class="hidden"><?=json_encode($F->faircoind->validateaddress($address))?></textarea><?



?><hr><h2>Block</h2><?
?><h4 onclick="$(this).next().toggleClass('hidden')">Faircoin->faircoind-><b>getbestblockhash()</b></h4><?
$blockhash=$F->faircoind->getbestblockhash();
?><textarea class="hidden"><?=json_encode($blockhash)?></textarea><?

?><h4 onclick="$(this).next().toggleClass('hidden')">Faircoin->faircoind-><b>getblock($blockhash)</b></h4><?
?><textarea class="hidden"><?=json_encode($F->faircoind->getblock($blockhash))?></textarea><?

?><h4 onclick="$(this).next().toggleClass('hidden')">Faircoin->faircoind-><b>getblockchaininfo()</b></h4><?
?><textarea class="hidden"><?=json_encode($F->faircoind->getblockchaininfo())?></textarea><?

?><h4 onclick="$(this).next().toggleClass('hidden')">Faircoin->faircoind-><b>getblockcount()</b></h4><?
$index= $F->faircoind->getblockcount();
?><textarea class="hidden"><?=json_encode($index)?></textarea><?

?><h4 onclick="$(this).next().toggleClass('hidden')">Faircoin->faircoind-><b>getblockhash($index)</b></h4><?
?><textarea class="hidden"><?=json_encode($F->faircoind->getblockhash($index))?></textarea><?

?><h4 onclick="$(this).next().toggleClass('hidden')">Faircoin->faircoind-><b>getblockheader($blockhash)</b></h4><?
?><textarea class="hidden"><?=json_encode($F->faircoind->getblockheader($blockhash))?></textarea><?

?><h4 onclick="$(this).next().toggleClass('hidden')">Faircoin->faircoind-><b>getchaintips()</b></h4><?
?><textarea class="hidden"><?=json_encode($F->faircoind->getchaintips())?></textarea><?


?><hr><h2>Transaction</h2><?
?><h4 onclick="$(this).next().toggleClass('hidden')">Faircoin->electrumfair-><b>deserialize($tx_hex)</b></h4><?
?><textarea class="hidden"><?=json_encode($F->electrumfair->deserialize($tx['hex']))?></textarea><?

?><h4 onclick="$(this).next().toggleClass('hidden')">Faircoin->electrumfair-><b>gettransaction($txid)</b></h4><?
$tx=$F->electrumfair->gettransaction($txid);
?><textarea class="hidden"><?=json_encode($tx)?></textarea><?

?><h4 onclick="$(this).next().toggleClass('hidden')">Faircoin->faircoind-><b>getrawtransaction($txid)</b></h4><?
$tx_hex=$F->faircoind->getrawtransaction($txid)?></textarea><?
?><textarea class="hidden"><?=json_encode($tx_hex);

?><h4 onclick="$(this).next().toggleClass('hidden')">Faircoin->faircoind-><b>decoderawtransaction($tx_hex)</b></h4><?
?><textarea class="hidden"><?=json_encode($F->faircoind->decoderawtransaction($tx_hex))?></textarea><?

?><h4 onclick="$(this).next().toggleClass('hidden')">Faircoin->faircoind-><b>getmempoolinfo()</b></h4><?
?><textarea class="hidden"><?=json_encode($F->faircoind->getmempoolinfo())?></textarea><?

?><h4 onclick="$(this).next().toggleClass('hidden')">Faircoin->faircoind-><b>getrawmempool()</b></h4><?
?><textarea class="hidden"><?=json_encode($F->faircoind->getrawmempool())?></textarea><?

?><h4 onclick="$(this).next().toggleClass('hidden')">Faircoin->faircoind-><b>gettxoutsetinfo()</b></h4><?
?><textarea class="hidden"><?=json_encode($F->faircoind->gettxoutsetinfo())?></textarea><?


?><hr><h2>Network</h2><?
?><h4 onclick="$(this).next().toggleClass('hidden')">Faircoin->electrumfair-><b>getservers()</b></h4><?
?><textarea class="hidden"><?=json_encode($F->electrumfair->getservers())?></textarea><?

?><h4 onclick="$(this).next().toggleClass('hidden')">Faircoin->electrumfair-><b>getfeerate()</b></h4><?
?><textarea class="hidden"><?=json_encode($F->electrumfair->getfeerate())?></textarea><?

?><h4 onclick="$(this).next().toggleClass('hidden')">Faircoin->faircoind-><b>getchainparameters()</b></h4><?
?><textarea class="hidden"><?=json_encode($F->faircoind->getchainparameters())?></textarea><?

?><h4 onclick="$(this).next().toggleClass('hidden')">Faircoin->faircoind-><b>getconnectioncount()</b></h4><?
?><textarea class="hidden"><?=json_encode($F->faircoind->getconnectioncount())?></textarea><?

?><h4 onclick="$(this).next().toggleClass('hidden')">Faircoin->faircoind-><b>getnettotals()</b></h4><?
?><textarea class="hidden"><?=json_encode($F->faircoind->getnettotals())?></textarea><?

?><h4 onclick="$(this).next().toggleClass('hidden')">Faircoin->faircoind-><b>getnetworkinfo()</b></h4><?
?><textarea class="hidden"><?=json_encode($F->faircoind->getnetworkinfo())?></textarea><?

?><h4 onclick="$(this).next().toggleClass('hidden')">Faircoin->faircoind-><b>getpeerinfo()</b></h4><?
?><textarea class="hidden"><?=json_encode($F->faircoind->getpeerinfo())?></textarea><?

?><h4 onclick="$(this).next().toggleClass('hidden')">Faircoin->faircoind-><b>estimatefee()</b></h4><?
?><textarea class="hidden"><?=json_encode($F->faircoind->estimatefee())?></textarea><?

?><h4 onclick="$(this).next().toggleClass('hidden')">Faircoin->faircoind-><b>getactiveadmins()</b></h4><?
?><textarea class="hidden"><?=json_encode($F->faircoind->getactiveadmins())?></textarea><?

?><h4 onclick="$(this).next().toggleClass('hidden')">Faircoin->faircoind-><b>getactivecvns()</b></h4><?
?><textarea class="hidden"><?=json_encode($F->faircoind->getactivecvns())?></textarea><?

?><h4 onclick="$(this).next().toggleClass('hidden')">Faircoin->faircoind-><b>getinfo()</b></h4><?
?><textarea class="hidden"><?=json_encode($F->faircoind->getinfo())?></textarea><?

?><hr><h2>Wallet</h2><?
?><h5>Wallet status</h5><?
?><h4 onclick="$(this).next().toggleClass('hidden')">Faircoin->electrumfair-><b>is_synchronized()</b></h4><?
?><textarea class="hidden"><?=json_encode($F->electrumfair->is_synchronized())?></textarea><?

?><h5>Payment requests</h5><?
?><h4 onclick="$(this).next().toggleClass('hidden')">Faircoin->electrumfair-><b>addrequest($amount,$memo)</b></h4><?
?><textarea class="hidden"><?=json_encode($F->electrumfair->addrequest(1,'mymemo'))?></textarea><?

?><h4 onclick="$(this).next().toggleClass('hidden')">Faircoin->electrumfair-><b>listrequests()</b></h4><?
$request=$F->electrumfair->listrequests()[0]['address'];
?><textarea class="hidden"><?=json_encode($F->electrumfair->listrequests())?></textarea><?

?><h4 onclick="$(this).next().toggleClass('hidden')">Faircoin->electrumfair-><b>getrequest($request)</b></h4><?
?><textarea class="hidden"><?=json_encode($F->electrumfair->getrequest($request))?></textarea><?

?><h4 onclick="$(this).next().toggleClass('hidden')">Faircoin->electrumfair-><b>rmrequest($request)</b></h4><?
?><textarea class="hidden"><?=json_encode($F->electrumfair->rmrequest($request))?></textarea><?

?><h4 onclick="$(this).next().toggleClass('hidden')">Faircoin->electrumfair-><b>clearrequests()</b></h4><?
?><textarea class="hidden"><?=json_encode($F->electrumfair->clearrequests())?></textarea><?

?><h5>Wallet addresses</h5><?
?><h4 onclick="$(this).next().toggleClass('hidden')">Faircoin->electrumfair-><b>createnewaddress()</b></h4><?
?><textarea class="hidden"><?=json_encode($F->electrumfair->createnewaddress())?></textarea><?

?><h4 onclick="$(this).next().toggleClass('hidden')">Faircoin->electrumfair-><b>getunusedaddress()</b></h4><?
?><textarea class="hidden"><?=json_encode($F->electrumfair->getunusedaddress())?></textarea><?

?><h4 onclick="$(this).next().toggleClass('hidden')">Faircoin->electrumfair-><b>ismine($address)</b></h4><?
?><textarea class="hidden"><?=json_encode($F->electrumfair->ismine($address))?></textarea><?

?><h4 onclick="$(this).next().toggleClass('hidden')">Faircoin->electrumfair-><b>listaddresses()</b></h4><?
?><textarea class="hidden"><?=json_encode($F->electrumfair->listaddresses())?></textarea><?

?><h4 onclick="$(this).next().toggleClass('hidden')">Faircoin->electrumfair-><b>listunspent()</b></h4><?
?><textarea class="hidden"><?=json_encode($F->electrumfair->listunspent())?></textarea><?

?><h4 onclick="$(this).next().toggleClass('hidden')">Faircoin->electrumfair-><b>notify($address,$URL)</b></h4><?
?><textarea class="hidden"><?=json_encode($F->electrumfair->notify($address,$URL))?></textarea><?

?><h4 onclick="$(this).next().toggleClass('hidden')">Faircoin->electrumfair-><b>freeze($address)</b></h4><?
?><textarea class="hidden"><?=json_encode($F->electrumfair->freeze($address))?></textarea><?

?><h4 onclick="$(this).next().toggleClass('hidden')">Faircoin->electrumfair-><b>unfreeze($address)</b></h4><?
?><textarea class="hidden"><?=json_encode($F->electrumfair->unfreeze($address))?></textarea><?

?><h5>Wallet history & balance</h5><?
?><h4 onclick="$(this).next().toggleClass('hidden')">Faircoin->electrumfair-><b>getbalance()</b></h4><?
?><textarea class="hidden"><?=json_encode($F->electrumfair->getbalance())?></textarea><?

?><h4 onclick="$(this).next().toggleClass('hidden')">Faircoin->electrumfair-><b>history()</b></h4><?
?><textarea class="hidden"><?=json_encode($F->electrumfair->history())?></textarea><?



?><h5>Wallet public keys</h5><?
?><h4 onclick="$(this).next().toggleClass('hidden')">Faircoin->electrumfair-><b>getmpk()</b></h4><?
?><textarea class="hidden"><?=json_encode($F->electrumfair->getmpk())?></textarea><?

?><h4 onclick="$(this).next().toggleClass('hidden')">Faircoin->electrumfair-><b>getpubkeys($address)</b></h4><?
?><textarea class="hidden"><?=json_encode($F->electrumfair->getpubkeys($address))?></textarea><?





?>
</body>
</html>
