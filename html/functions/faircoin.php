<?php

/**
 *
 */

define('ELECTRUMFAIR_RPC','http://user:password@faircoin:7777');
define('FAIRCOIND_RPC',   'http://user:password@faircoin:8332');

function regex($s,$p){
  return preg_match('/'.$p.'/',$s);
}

class faircoind
{

  public $param=Array( 'jsonrpc' => '2.0', 'id' => 'curltext' );

  function faircoind(){ return $this->rpccall(FAIRCOIND_RPC); }
  function rpccall($url){

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url );
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POST,true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode( $this->param ) );

    $data = curl_exec($ch);
    curl_close($ch);

    return $data;

  }

  ### faircoind methods / blockchain ####
  function getbestblockhash(){
    $this->param['method']=__FUNCTION__;
    $this->param['params']=Array();
    return json_decode( $this->faircoind(), true )['result'];
  }
  function getblock($blockhash){
    $this->param['method']=__FUNCTION__;
    $this->param['params']=Array( $blockhash );
    return json_decode( $this->faircoind(), true )['result'];
  }
  function getblockchaininfo(){
    $this->param['method']=__FUNCTION__;
    $this->param['params']=Array();
    return json_decode( $this->faircoind(), true )['result'];
  }
  function getblockcount(){
    $this->param['method']=__FUNCTION__;
    $this->param['params']=Array();
    return json_decode( $this->faircoind(), true )['result'];
  }
  function getblockhash($index){
    $this->param['method']=__FUNCTION__;
    $this->param['params']=Array( $index );
    return json_decode( $this->faircoind(), true )['result'];
  }
  function getblockheader($blockhash){
    $this->param['method']=__FUNCTION__;
    $this->param['params']=Array( $blockhash );
    return json_decode( $this->faircoind(), true )['result'];
  }
  function getchaintips(){
    $this->param['method']=__FUNCTION__;
    $this->param['params']=Array();
    return json_decode( $this->faircoind(), true )['result'];
  }
  function getmempoolinfo(){
    $this->param['method']=__FUNCTION__;
    $this->param['params']=Array();
    return json_decode( $this->faircoind(), true )['result'];
  }
  function getrawmempool(){
    $this->param['method']=__FUNCTION__;
    $this->param['params']=Array();
    return json_decode( $this->faircoind(), true )['result'];
  }
  function gettxoutsetinfo(){
    $this->param['method']=__FUNCTION__;
    $this->param['params']=Array();
    return json_decode( $this->faircoind(), true )['result'];
  }

  ### faircoind methods / control ####
  function getinfo(){
    $this->param['method']=__FUNCTION__;
    $this->param['params']=Array();
    return json_decode( $this->faircoind(), true )['result'];
  }

  ### faircoind methods / cvn ####
  function estimatefee(){
    $this->param['method']=__FUNCTION__;
    $this->param['params']=Array(1);
    return json_decode( $this->faircoind(), true )['result'];
  }
  function getactiveadmins(){
    $this->param['method']=__FUNCTION__;
    $this->param['params']=Array();
    return json_decode( $this->faircoind(), true )['result'];
  }
  function getactivecvns(){
    $this->param['method']=__FUNCTION__;
    $this->param['params']=Array();
    return json_decode( $this->faircoind(), true )['result'];
  }
  function getchainparameters(){
    $this->param['method']=__FUNCTION__;
    $this->param['params']=Array();
    return json_decode( $this->faircoind(), true )['result'];
  }
  function getconnectioncount(){
    $this->param['method']=__FUNCTION__;
    $this->param['params']=Array();
    return json_decode( $this->faircoind(), true )['result'];
  }
  function getnettotals(){
    $this->param['method']=__FUNCTION__;
    $this->param['params']=Array();
    return json_decode( $this->faircoind(), true )['result'];
  }
  function getnetworkinfo(){
    $this->param['method']=__FUNCTION__;
    $this->param['params']=Array();
    return json_decode( $this->faircoind(), true )['result'];
  }
  function getpeerinfo(){
    $this->param['method']=__FUNCTION__;
    $this->param['params']=Array();
    return json_decode( $this->faircoind(), true )['result'];
  }

  ### faircoind methods / transaction ###
  function decoderawtransaction($tx_hex){
    $this->param['method']=__FUNCTION__;
    $this->param['params']=Array($tx_hex);
    return json_decode( $this->faircoind(), true )['result'];
  }
  function getrawtransaction($txid){
    $this->param['method']=__FUNCTION__;
    $this->param['params']=Array($txid);
    return json_decode( $this->faircoind(), true )['result'];
  }

  ### faircoin methods / util ###
  function validateaddress($address){
    $this->param['method']=__FUNCTION__;
    $this->param['params']=Array($address);
    return json_decode( $this->faircoind(), true )['result'];
  }
  function validatepubkey($pubkey){
    $this->param['method']=__FUNCTION__;
    $this->param['params']=Array($pubkey);
    return json_decode( $this->faircoind(), true )['result'];
  }
  function verifymessage($address, $message, $signature){
    $this->param['method']=__FUNCTION__;
    $this->param['params']=Array( $address, $signature, $message );
    return json_decode( $this->faircoind(), true )['result'];
  }

}

class electrumfair
{
  public $param=Array( 'jsonrpc' => '2.0', 'id' => 'curltext' );

  function electrumfair(){ return $this->rpccall(ELECTRUMFAIR_RPC); }
  function rpccall($url){

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url );
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POST,true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode( $this->param ) );

    $data = curl_exec($ch);
    curl_close($ch);

    return $data;

  }

  ### electrumfair methods / messages ###
  /*
  encrypt decrypt still not working by jsonrpc or commandline

  function decrypt($pubkey, $encrypted){
    $this->param['method']=__FUNCTION__;
    $this->param['params']=Array( 'pubkey' => $pubkey, 'encrypted' => $encrypted, 'password' => getenv('PASSWORD') );
    return json_decode( $this->electrumfair(), true )['result'];
  }
  function encrypt($pubkey, $message){
    $this->param['method']=__FUNCTION__;
    $this->param['params']=Array( 'pubkey' => $pubkey, 'message' => $message );
    return json_decode( $this->electrumfair(), true )['result'];
  }
  */
  function signmessage($address, $message, $password=false){
    $this->param['method']=__FUNCTION__;
    $this->param['params']=Array( 'address' => $address, 'message' => $message, 'password' => ( !$password ? getenv('PASSWORD') : $password ) );
    return json_decode( $this->electrumfair(), true )['result'];
  }
  function verifymessage($address, $message, $signature){
    $this->param['method']=__FUNCTION__;
    $this->param['params']=Array( 'address' => $address, 'message' => $message, 'signature' => $signature );
    return json_decode( $this->electrumfair(), true )['result'];
  }

  ### electrumfair methods / address ###
  function getaddressbalance($address){
    $this->param['method']=__FUNCTION__;
    $this->param['params']=Array( 'address' => $address );
    return json_decode( $this->electrumfair(), true )['result'];
  }
  function getaddresshistory($address){
    $this->param['method']=__FUNCTION__;
    $this->param['params']=Array( 'address' => $address );
    $result=json_decode( $this->electrumfair(), true )['result'];
    return $result;
  }
  function getaddressunspent($address){
    $this->param['method']=__FUNCTION__;
    $this->param['params']=Array( 'address' => $address );
    $result=json_decode( $this->electrumfair(), true )['result'];
    return $result;
  }

  ### electrumfair methods / transaction ###
  function serialize($tx_json){
    $this->param['method']=__FUNCTION__;
    $this->param['params']=Array( 'jsontx' => $tx_json );
    $result=json_decode( $this->electrumfair(), true )['result'];
    return $result;
  }
  function deserialize($tx_hex){
    $this->param['method']=__FUNCTION__;
    $this->param['params']=Array( 'tx' => $tx_hex );
    $result=json_decode( $this->electrumfair(), true )['result'];
    return $result;
  }
  function gettransaction($txid){
    $this->param['method']=__FUNCTION__;
    $this->param['params']=Array( 'txid' => $txid );
    $result=json_decode( $this->electrumfair(), true )['result'];
    return $result;
  }

  ### electrumfair methods / network ###
  function getservers(){
    $this->param['method']=__FUNCTION__;
    $this->param['params']=Array();
    $result=json_decode( $this->electrumfair(), true )['result'];
    return $result;
  }
  function getfeerate(){
    $this->param['method']=__FUNCTION__;
    $this->param['params']=Array();
    $result=json_decode( $this->electrumfair(), true )['result'];
    return $result;
  }

  ### electrumfair methods / util ###
  function is_synchronized(){
    $this->param['method']=__FUNCTION__;
    $this->param['params']=Array();
    return json_decode( $this->electrumfair(), true )['result'];
  }

  function validateaddress($address){
    $this->param['method']=__FUNCTION__;
    $this->param['params']=Array($address);
    return json_decode( $this->electrumfair(), true )['result'];
  }
  function version(){
    $this->param['method']=__FUNCTION__;
    $this->param['params']=Array();
    return json_decode( $this->electrumfair(), true )['result'];
  }

  ### electrumfair methods / wallet ###
  function addrequest($amount,$memo){
    $this->param['method']=__FUNCTION__;
    $this->param['params']=Array('amount' => $amount, 'memo' => $memo);
    return json_decode( $this->electrumfair(), true )['result'];
  }
  function getrequest($request){
    $this->param['method']=__FUNCTION__;
    $this->param['params']=Array($request);
    return json_decode( $this->electrumfair(), true )['result'];
  }
  function listrequests(){
    $this->param['method']=__FUNCTION__;
    $this->param['params']=Array();
    return json_decode( $this->electrumfair(), true )['result'];
  }
  function rmrequest($address){
    $this->param['method']=__FUNCTION__;
    $this->param['params']=Array('address' => $address);
    return json_decode( $this->electrumfair(), true )['result'];
  }
  function clearrequests(){
    $this->param['method']=__FUNCTION__;
    $this->param['params']=Array();
    return json_decode( $this->electrumfair(), true )['result'];
  }

  function createnewaddress(){
    $this->param['method']=__FUNCTION__;
    $this->param['params']=Array();
    return json_decode( $this->electrumfair(), true )['result'];
  }
  function getunusedaddress(){
    $this->param['method']=__FUNCTION__;
    $this->param['params']=Array();
    return json_decode( $this->electrumfair(), true )['result'];
  }
  function ismine($address){
    $this->param['method']=__FUNCTION__;
    $this->param['params']=Array('address' => $address);
    return json_decode( $this->electrumfair(), true )['result'];
  }
  function listaddresses(){
    $this->param['method']=__FUNCTION__;
    $this->param['params']=Array();
    return json_decode( $this->electrumfair(), true )['result'];
  }
  function listunspent(){
    $this->param['method']=__FUNCTION__;
    $this->param['params']=Array();
    return json_decode( $this->electrumfair(), true )['result'];
  }
  function freeze($address){
    $this->param['method']=__FUNCTION__;
    $this->param['params']=Array('address' => $address);
    return json_decode( $this->electrumfair(), true )['result'];
  }
  function unfreeze($address){
    $this->param['method']=__FUNCTION__;
    $this->param['params']=Array('address' => $address);
    return json_decode( $this->electrumfair(), true )['result'];
  }
  function notify($address, $URL){
    $this->param['method']=__FUNCTION__;
    $this->param['params']=Array('address' => $address, 'URL' => $URL );
    return json_decode( $this->electrumfair(), true )['result'];
  }

  function getbalance(){
    $this->param['method']=__FUNCTION__;
    $this->param['params']=Array();
    return json_decode( $this->electrumfair(), true )['result'];
  }
  function history(){
    $this->param['method']=__FUNCTION__;
    $this->param['params']=Array();
    return json_decode( json_decode( $this->electrumfair(), true )['result'], true );
  }

  function getmpk(){
    $this->param['method']=__FUNCTION__;
    $this->param['params']=Array();
    return json_decode( $this->electrumfair(), true )['result'];
  }
  function getpubkeys($address){
    $this->param['method']=__FUNCTION__;
    $this->param['params']=Array('address' => $address);
    return json_decode( $this->electrumfair(), true )['result'];
  }

  function payto($address, $amount, $password=false ){
    $this->param['method']=__FUNCTION__;
    $this->param['params']=Array('destination' => $address, 'amount' => $amount, 'password' => ( !$password ? getenv('PASSWORD') : $password ), 'unsigned' => true );
    return json_decode( $this->electrumfair(), true );
  }
  function paytomany($Payment, $password=false ){
    $this->param['method']=__FUNCTION__;
    $this->param['params']=Array('outputs' => $Payment, 'password' => ( !$password ? getenv('PASSWORD') : $password ) );
    return json_decode( $this->electrumfair(), true );
  }
  function signtransaction($serialized_tx, $password=false ){
    $this->param['method']=__FUNCTION__;
    $this->param['params']=Array('tx' => $serialized_tx, 'password' => ( !$password ? getenv('PASSWORD') : $password ) );
    return json_decode( $this->electrumfair(), true );
  }
  function broadcast($serialized_tx){
    $this->param['method']=__FUNCTION__;
    $this->param['params']=Array('tx' => $serialized_tx);
    return json_decode( $this->electrumfair(), true )['result'];
  }

}

class Faircoin
{

  public $online=false;
  public $faircoind=false;
  public $electrumfair=false;

  function __construct(){
    $this->faircoind = new faircoind();
    $this->electrumfair = new electrumfair();
    $this->online = !empty( $this->electrumfair->getservers() );
  }

}
